// ca async-process dirs
use anyhow::Result;
use async_process::Command;
use async_std::{path::Path, fs};

pub async fn simple() -> Result<()> {
    let cmd = Command::new("sudo").arg("ls").output().await?;
    dbg!(cmd);
    Ok(())
}

pub async fn install() -> Result<()> {
    // build commands
    let out = format!("{:?}/bin", dirs::home_dir().unwrap());
    let url = "https://github.com/jarun/nnn";
    let bin = "nnn";
    let dest = format!("/tmp/{}", bin);
    let cmds = &[
        format!("git clone {} {}", url, dest),
        format!("cd {}", dest),
        "make O_NERD=1 O_PCRE=1".to_string(),
        format!("mv nnn {}", out)
    ].join(" && ");
    // check directory
    if Path::new(&dest).exists().await {
        fs::remove_dir_all(&dest).await?;
    }
    dbg!(cmds);
    // run commands
    let cmd = Command::new("ion")
        .arg("-c")
        .arg(cmds)
        .output()
        .await?;
    dbg!(cmd);
    Ok(())
}
