use anyhow::Result;

pub async fn heap() -> Result<()> {
    let heap_vector: Vec<i8> = Vec::new();
    let heap_string: String = String::from("Hi mom");
    let heap_i8: Box<i8> = Box::new(30);
    dbg!(heap_vector, heap_string, heap_i8);
    Ok(())
}

// pub async fn owned() -> Result<()> {
//     let stack_i8: i8 = 10;
//     let heap_vector: Vec<i8> = Vec::new();
//     let heap_string: String = String::from("Hi mom");
//     // OWNERSHIP OF HEAP ALLOCATED MEMORY ASSIGNED TO heap_i8
//     let heap_i8: Box<i8> = Box::new(30); 
// 
//     let stack_i8_2 = stack_i8;
//     println!("{}", stack_i8);
//     println!("{}", stack_i8_2);
// 
//     let heap_i8_2 = heap_i8; // TRANSFERED OWNERSHIP TO heap_i8_2
//     println!("{}", heap_i8); // FAIL: VALUE BORROWED HERE AFTER MOVE
//     println!("{}", heap_i8_2);
//     Ok(())
// }

pub async fn borrow() -> Result<()> {
    let stack_i8: i8 = 10;
    // OWNERSHIP OF HEAP ALLOCATED MEMORY ASSIGNED TO heap_i8
    let heap_i8: Box<i8> = Box::new(30); 

    let stack_i8_2 = stack_i8;
    println!("{}", stack_i8);
    println!("{}", stack_i8_2);

    let heap_i8_2 = &heap_i8; // BORROW OWNERSHIP TO heap_i8_2
    println!("{}", heap_i8); // SUCCESS: VALUE BORROWED HERE AFTER MOVE
    println!("{}", heap_i8_2);
    Ok(())
}

pub async fn clone() -> Result<()> {
    let stack_i8: i8 = 10;
    // OWNERSHIP OF HEAP ALLOCATED MEMORY ASSIGNED TO heap_i8
    let heap_i8: Box<i8> = Box::new(30); 

    let stack_i8_2 = stack_i8;
    println!("{}", stack_i8);
    println!("{}", stack_i8_2);

    let heap_i8_2 = heap_i8.clone(); // CLONED OWNERSHIP TO heap_i8_2
    println!("{}", heap_i8); // SUCCESS: VALUE BORROWED HERE AFTER MOVE
    println!("{}", heap_i8_2);
    Ok(())
}

pub async fn params() -> Result<()> {
    let stack_f64: f64 = 1.;
    let heap_f64: Box<f64> = Box::new(2.);

    stack_function(stack_f64).await?;
    // In main stack 1
    println!("In main stack {}", stack_f64);
    Ok(())
}

async fn stack_function(mut param: f64) -> Result<()> {
    param += 9.;
    // In stack_procedure with param 10
    println!("In stack_procedure with param {}", param);
    Ok(())
}

async fn heap_function(mut param: f64) -> Result<()> {
    param += 9.;
    // In stack_procedure with param 10
    println!("In stack_procedure with param {}", param);
    Ok(())
}
