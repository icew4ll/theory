use anyhow::Result;

pub async fn raw() -> Result<()> {
    let shader = r#"
        #version 330

        in vec4 v_color;
        out vec4 color;

        void main() {
            color = v_color;
        };
    "#;
    // arbitrary number of hashes can be used as delimeter
    let crazy_raw_string = r###"
        My fingers #"
        can#"#t stop "#"" hitting
        hash##"#
    "###;
    println!("{}{}", shader, crazy_raw_string);
    Ok(())
}
