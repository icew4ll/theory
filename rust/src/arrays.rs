use anyhow::Result;
use async_std::fs::File;
use async_std::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Cfg {
    installs: Vec<Bin>,
}
#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Bin {
    url: String,
    bin: String,
    cmds: Vec<String>,
}

pub async fn simple() -> anyhow::Result<()> {
    let array = [1, 4, 3, 2, 2];
    let array_iter = std::array::IntoIter::new(array).into_iter();
    array_iter.for_each(|x| println!("{}", x));
    // let res: Vec<_> = array_iter.map(|x| x + 1).rev().collect();
    // dbg!(res);
    Ok(())
}

pub async fn yaml() -> Result<()> {
    // 1) open file
    let mut file = File::open("data.yaml").await?;
    let mut buffer = String::new();
    file.read_to_string(&mut buffer).await?;

    // 2) Deserialize yaml string into rust type
    let deserialized: Cfg = serde_yaml::from_str(&buffer)?;

    // 3) convert array to iterator
    let array_iter = deserialized.installs.into_iter();

    // filter array and collect into vec
    dbg!(array_iter.filter(|x| x.bin == "nnn" ).collect::<Vec<_>>());
    Ok(())
}
