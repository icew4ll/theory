use anyhow::Result;
mod arrays;
mod cli;
mod command;
mod strings;
mod ownership;

#[async_std::main]
async fn main() -> Result<()> {
    ownership::params().await?;
    Ok(())
}
