// cargo add clap --features yaml

use anyhow::Result;

pub async fn args() -> Result<()> {
    // 1) import clap yaml deps
    use clap::{load_yaml, App};

    // 2) load yaml
    let yaml = load_yaml!("data/args.yml");
    let m = App::from(yaml).get_matches();

    // get value of argument "repo"
    if let Some(repo) = m.value_of("repo") {
        println!("Selected {}", repo);
    } else {
        println!("--repo wasn't used...");
    }
    Ok(())
}
